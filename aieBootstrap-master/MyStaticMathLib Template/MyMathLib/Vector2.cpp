#include "Vector2.h"
#include <math.h> 


Vector2::Vector2() : x(0), y(0)
{
}

Vector2::Vector2(float a_x, float a_y) : x(a_x), y(a_y) 
//Vector2 (2-dimensional using 2 floats)
{
}

Vector2::Vector2(const Vector2 & a_rhs) : x(a_rhs.x), y(a_rhs.y)
{
}

#pragma region Single + - * / = * / Operator
Vector2 Vector2::operator + (const Vector2& a_rhs) const
//V = V + V (point translated by a vector)
{
	return Vector2(x + a_rhs.x, y + a_rhs.y);
}

Vector2 Vector2::operator - (const Vector2& a_rhs) const
//V = V � V (point translated by a vector)
{
	return Vector2(x - a_rhs.x, y - a_rhs.y);
}

Vector2 Vector2::operator * (float a_scalar) const
//V = V x f (vector scale)
{
	return Vector2(x * a_scalar, y * a_scalar);
}

	Vector2 Vector2::operator / (float a_scalar) const
 {
	return Vector2(x / a_scalar, y / a_scalar);
 }

Vector2 Vector2::operator = (const Vector2& a_rhs)
{
	x = a_rhs.x;
	y = a_rhs.y;
	return *this;
}

//Vector2 operator / (float a_scalar, const Vector2& a_rhs)
//{
//	return Vector2(a_rhs.x / a_scalar, a_rhs.y / a_scalar, a_rhs.z / a_scalar);
//}

#pragma endregion

#pragma region Multi =+ -= *= /= *= /= Operator
Vector2& Vector2::operator += (const Vector2& a_rhs)
{
	x += a_rhs.x;
	y += a_rhs.y;
	return *this;
}

Vector2& Vector2::operator -= (const Vector2& a_rhs)
{
	x -= a_rhs.x;
	y -= a_rhs.y;
	return *this;
}

Vector2& Vector2::operator *= (float a_scalar)
//V = f x V (vector scale)
{
	x *= a_scalar;
	y *= a_scalar;
	return *this;
}

// Vector2& Vector2::operator /= (float a_scalar)
//{
//	x /= a_scalar;
//	y /= a_scalar;
//	return *this;
//}



#pragma endregion

#pragma region Mag, norm, getnorm, dot

float Vector2::magnitude()
//f = V.magnitude()
{
	//� float f = cosf(30); 
	return float(sqrtf(x*x + y*y));
}

void Vector2::normalise() //� Make object a normal
//V.normalise()
{
	float mag = magnitude();
	if (mag == 0);
	x /= mag;
	y /= mag;

}

Vector2 Vector2::getNormal() //� Return the object as a normal vector (do NOT make the object a normal)
{
	float mag = magnitude();
	if (mag == 0) return Vector2(x, y);
	return Vector2(x / mag, y / mag);

}

float Vector2::dot(const Vector2& a_rhs)
//f = V.dot( V )
{//�
	return x * a_rhs.x + y * a_rhs.y;
}

#pragma endregion

Vector2 operator * (float a_scalar, const Vector2& a_rhs)
{
	return Vector2(a_rhs.x * a_scalar, a_rhs.y * a_scalar);
}

#include "Vector4.h"
#include <math.h>
//#if 0
Vector4::Vector4() : x(0), y(0), z(0), w(0)
{ //�
}

Vector4::Vector4(float a_x, float a_y, float a_z, float a_w) : x(a_x), y(a_y), z(a_z), w(a_w)
//Vector4(4 - dimensional using 4 floats)
{ //�
}

//� void Vector4::get(float& a_x, float& a_y, float& a_z, float& a_w) const
//� {
//� }

void Vector4::set(const float& a_x, const float& a_y, const float& a_z, const float& a_w)
{ //�
	x = a_x;
	y = a_y;
	z = a_z;
	w = a_w;
}

#pragma region Single + - * / = Operator

Vector4 Vector4::operator + (const Vector4 &a_rhs) const
{ //�
	return Vector4(x + a_rhs.x, y + a_rhs.y, z + a_rhs.z, w + a_rhs.w);
}

Vector4 Vector4::operator - (const Vector4 &a_rhs) const
{ //�
	return Vector4(x - a_rhs.x, y - a_rhs.y, z - a_rhs.z, w - a_rhs.w);
}

Vector4 Vector4::operator * (const Vector4 &a_rhs) const
{ //�
	return Vector4(x * a_rhs.x, y * a_rhs.y, z * a_rhs.z, w * a_rhs.w);
}

Vector4 Vector4::operator / (const Vector4 &a_rhs) const
{ //�
	return Vector4(x / a_rhs.x, y / a_rhs.y, z / a_rhs.z, w / a_rhs.w);
}

Vector4 Vector4::operator * (const float a_rhs) const
{ //�
	return Vector4(x * a_rhs, y * a_rhs, z * a_rhs, w * a_rhs);
}

Vector4 Vector4::operator / (const float a_rhs) const
{ //�
	return Vector4(x / a_rhs, y / a_rhs, z / a_rhs, w / a_rhs);

}

#pragma endregion

#pragma region Multi =+ -= *= /= *= /= Operator
Vector4& Vector4::operator += (const Vector4 &a_rhs)
{ //�
	*this = *this + a_rhs;
	return *this;
}

Vector4& Vector4::operator -= (const Vector4 &a_rhs)
{ //�
	*this = *this - a_rhs;
	return *this;
}

Vector4& Vector4::operator *= (const Vector4 &a_rhs)
{ //�
	*this = *this * a_rhs;
	return *this;
}

Vector4& Vector4::operator /= (const Vector4 &a_rhs)
{ //�
	*this = *this / a_rhs;
	return *this;
}

Vector4& Vector4::operator *= (const float a_rhs)
{ //�
	*this = *this * a_rhs;
	return *this;
}

Vector4& Vector4::operator /= (const float a_rhs)
{ //�
	*this = *this / a_rhs;
	return *this;
}

#pragma endregion

bool Vector4::operator == (const Vector4 &a_rhs) const
{ //�
	return ((x == a_rhs.x) && (y == a_rhs.y) && (z == a_rhs.z) && (w == a_rhs.w));
}

bool Vector4::operator != (const Vector4 &a_rhs) const
{ //�
	return !((x == a_rhs.x) && (y == a_rhs.y) && (z == a_rhs.z) && (w == a_rhs.w));
}

Vector4 Vector4::operator -() const
{ //�
	return Vector4(-x, -y, -z, -w);
}

Vector4 operator * (float a_scalar, const Vector4& a_other)
{ //�
	return Vector4(a_other * a_scalar);
}

#pragma region Mag, norm, getnorm, dot, cross

float Vector4::magnitude() const
{ //�
	return sqrtf(x*x + y*y + z*z + w*w);
}

Vector4& Vector4::normalise()
{ //�
	float mag = magnitude();
	if (mag == 0.f) return *this;
	*this /= magnitude();
	return *this;
}

Vector4 Vector4::getNormalised()
{
	float mag = magnitude();
	if (mag == 0) return *this;  //� NB come back and check if changed vector3
	return (*this / magnitude());
}

float Vector4::dot(const Vector4 &a_rhs) const
{
	//float c1 = x * a_rhs.x + y * a_rhs.y + z * a_rhs.z + w * a_rhs.w;
	return (x * a_rhs.x + y * a_rhs.y + z * a_rhs.z + w * a_rhs.w);
}

Vector4 Vector4::cross(const Vector4 &a_rhs) const
{ //�
	return Vector4(/*x*/(y * a_rhs.z - z* a_rhs.y),/*y*/(z * a_rhs.x - x * a_rhs.z), /*z*/(x * a_rhs.y - y * a_rhs.x), /*w*/ (w));//(w * a_rhs.w) );
}

#pragma endregion

//#endif
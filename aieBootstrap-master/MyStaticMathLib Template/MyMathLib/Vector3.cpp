﻿#include "Vector3.h"
#include <math.h>

Vector3::Vector3() : x(0), y(0), z(0)
{ //§
}

Vector3::Vector3(float a_x, float a_y, float a_z) : x(a_x), y(a_y), z(a_z)
//Vector3 (3-dimensional using 3 floats)
{ //§
}

//§ void Vector3::get(float& a_x, float& a_y, float& a_z) const //§ Get but return Void???
//§ {
//§ }

void Vector3::set(const float& a_x, const float& a_y, const float& a_z) //§ Set values to fresh values.
{ //§
	x = a_x;
	y = a_y;
	z = a_z;
}

#pragma region Single + - * / = Operator

Vector3 Vector3::operator + (const Vector3 &a_rhs) const
//V = V + V (point translated by a vector)
{ //§
	return Vector3(x + a_rhs.x, y + a_rhs.y, z + a_rhs.z);
}

Vector3 Vector3::operator - (const Vector3 &a_rhs) const
//V = V – V (point translated by a vector)
{ //§
	return Vector3(x - a_rhs.x, y - a_rhs.y, z - a_rhs.z);
}

Vector3 Vector3::operator * (const Vector3 &a_rhs) const
{ //§
	return Vector3(x * a_rhs.x, y * a_rhs.y, z * a_rhs.z);
}

Vector3 Vector3::operator / (const Vector3 &a_rhs) const
{ //§
	return Vector3(x / a_rhs.x, y / a_rhs.y, z / a_rhs.z);
}

Vector3 Vector3::operator * (const float a_rhs) const
//V = V x f (vector scale)
{ //§
	return Vector3(x * a_rhs, y * a_rhs, z * a_rhs);
}

Vector3 Vector3::operator / (const float a_rhs) const
{ //§ 
	return Vector3(x / a_rhs, y / a_rhs, z / a_rhs);
}

Vector3 Vector3::operator = (const Vector3 &a_rhs) //const
{ //§
	//return *this;
	return Vector3(x = a_rhs.x, y = a_rhs.y, z = a_rhs.z);
}

#pragma endregion

#pragma region Multi =+ -= *= /= *= /= Operator
Vector3& Vector3::operator += (const Vector3 &a_rhs)
//V = f x V (vector scale)
{//§
	*this = *this + a_rhs;
	return *this;
}

Vector3& Vector3::operator -= (const Vector3 &a_rhs)
{//§
	*this = *this - a_rhs;
	return *this;
}

Vector3& Vector3::operator *= (const Vector3 &a_rhs)
{//§
	*this = *this * a_rhs;
	return *this;
}

Vector3& Vector3::operator /= (const Vector3 &a_rhs)
{//§
	*this = *this / a_rhs;
	return *this;
}

Vector3& Vector3::operator *= (const float a_rhs)
{//§
	*this = *this * a_rhs;
	return *this;
}

Vector3& Vector3::operator /= (const float a_rhs)
{//§
	*this = *this / a_rhs;
	return *this;
}

#pragma endregion

bool Vector3::operator == (const Vector3 &a_rhs) const
{ //§
	return ((x == a_rhs.x) && (y == a_rhs.y) && (z == a_rhs.z));
}

bool Vector3::operator != (const Vector3 &a_rhs) const
{ //§
	return !((x == a_rhs.x) && (y == a_rhs.y) && (z == a_rhs.z));
}

Vector3 Vector3::operator -() const //§ Inverse sign
{ //§
	return Vector3(-x, -y, -z);
}

Vector3 operator*(float a_scalar, const Vector3 & a_other)
{ //§
	return Vector3(a_other * a_scalar);
}

#pragma region Mag, norm, getnorm, dot, cross
float Vector3::magnitude() const
{ //§
	return sqrtf(x*x + y*y + z*z);
}

Vector3& Vector3::normalise()
{ //§
	float mag = magnitude();
	if (mag == 0.f) return *this;
	*this /= magnitude();
	return *this;
}

Vector3 Vector3::getNormalised()
{ //§

	float mag = magnitude();
	if (mag == 0) return *this; //§ NB come back and check
	 return (*this / magnitude());
}

float Vector3::dot(const Vector3 &a_rhs) const
{ //§
	
	return (x * a_rhs.x + y * a_rhs.y + z * a_rhs.z);
	
}

//			Vector3& Vector3::dotofNormalised(const Vector3 &a_rhs) const
//			{
//				//§ NB: Come back to this.
//				//§ Vector3.Dot( vector1.Normalize(), vector2.Normalize() )
//			}


//			float Vector3::dot3(const Vector3 &a_rhs, const Vector3 &b_rhs) const//dot product for 3 vectors.
//			{
//			
//			}

Vector3 Vector3::cross(const Vector3 & a_rhs) const 
{ //§
	return Vector3(/*x*/(y * a_rhs.z - z* a_rhs.y),/*y*/(z * a_rhs.x - x * a_rhs.z), /*z*/(x * a_rhs.y - y * a_rhs.x));
}

#pragma endregion
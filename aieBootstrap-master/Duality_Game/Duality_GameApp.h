#pragma once

#include "Application.h"
#include "Renderer2D.h"
#include "Audio.h"
#include "Matrix3.h"
#include "Vector3.h"
#include "memory"
#include <vector>

using std::vector;

class Duality_GameApp : public aie::Application {
public:

	Duality_GameApp();
	virtual ~Duality_GameApp();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

protected:
	float m_playerX, m_playerY, mTimer;
	bool mGameIsOver;

	aie::Renderer2D*	m_2dRenderer;
	aie::Font*			m_font;
	//vector<Planet> planets;
	std::unique_ptr<aie::Renderer2D> m_2dRenderer;
	std::shared_ptr<aie::Font>		m_font;

	std::shared_ptr<aie::Texture> m_starsTex;
	std::shared_ptr<aie::Texture> m_sunTex;
};
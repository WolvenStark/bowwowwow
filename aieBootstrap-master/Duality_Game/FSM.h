#pragma once

#include <vector>
#include "Matrix3.h"
#include "Vector3.h"
#include <memory>

using std::vector;

class FSM
{
public:
	FSM();

	//~FSM();
	virtual ~FSM();

	FSM(int StateCount);
	void addstate(int id, State* newState);
	void forcestate(int id) ;
	void update(GameObject * gameobject, float deltatime);


protected:
	State* m_currentState = nullptr;
	std::vector<State*> m_states;

	//std::vector<Moon> moons;
};


#include "Duality_GameApp.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"

Duality_GameApp::Duality_GameApp() {

}

Duality_GameApp::~Duality_GameApp() {

}

bool Duality_GameApp::startup() {
	
	//Vector3 center = { (float)getWindowWidth() / 2, (float)getWindowHeight() / 2, 1 };
	
	return true;
}

void Duality_GameApp::shutdown() {

}

void Duality_GameApp::update(float deltaTime) {

	// input example
	aie::Input* input = aie::Input::getInstance();
	
}

void Duality_GameApp::draw() {

	// wipe the screen to the background colour
	clearScreen();

	// begin drawing sprites
	m_2dRenderer->begin();

	// draw your stuff here!
	
	// output some text, uses the last used colour
	m_2dRenderer->drawText(m_font, "Press ESC to quit", 0, 0);

	// done drawing sprites
	m_2dRenderer->end();
}
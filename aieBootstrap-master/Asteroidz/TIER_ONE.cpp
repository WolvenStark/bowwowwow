#pragma once
#include "TIER_ONE.h"
//#include "T_ONE_POWERUP.h"
TIER_ONE::TIER_ONE() : t_X(0), t_Y(0)
{}

TIER_ONE::TIER_ONE(float newX, float newY, float newRadius, int newHealth, int newColour) 
	: t_X(newX), t_Y(newY), t_Radius(newRadius), t_AmIAlive(true), t_Health(newHealth),
	/*max health?*/ t_Colour(newColour){}

TIER_ONE::~TIER_ONE()
{
}

#pragma region X Y RADIUS
float	TIER_ONE::getXvalue() 
{
	return t_X;
}

void	TIER_ONE::setXvalue(float Fresh_X_Value)
{
	t_X = Fresh_X_Value;
}

float	TIER_ONE::getYvalue()
{
	return t_Y;
}

void	TIER_ONE::setYvalue(float Fresh_Y_Value)
{
	t_Y = Fresh_Y_Value;
}

float	TIER_ONE::getRadius()
{
	return t_Radius;
}

void	TIER_ONE::setRadius(float Fresh_Radius_Value)
{
	t_Radius = Fresh_Radius_Value;
}
#pragma endregion

#pragma region Health & Death
bool	TIER_ONE::isItAlive()
{
	return t_AmIAlive;
}

void	TIER_ONE::setAliveStatus(bool Fresh_IsItAlive_Value)
{
	t_AmIAlive = Fresh_IsItAlive_Value;
}

int		TIER_ONE::getHealth() const 
{
	return t_Health;
}

void	TIER_ONE::healMe(int healAmount)
{//Health cannot breach it's maximum or minimum!
	t_Health += healAmount;
	if (t_Health > t_MaxHealth) 
	{
		t_Health = t_MaxHealth;
	}
}

void	TIER_ONE::damageMe(int amt) {
	t_Health -= amt;
	if (t_Health < 0) {
		t_Health = 0;
		t_AmIAlive = false;
	}
}
#pragma endregion

#pragma region Colours & Render
int		TIER_ONE::getColour()
{
	return t_Colour;
}

void	TIER_ONE::setColour(int Fresh_Colour_Value)
{
	t_Colour = Fresh_Colour_Value;
}

void	TIER_ONE::render(aie::Renderer2D* a_r2d) {
	a_r2d->setRenderColour(getColour());
	a_r2d->drawCircle(getXvalue(), getYvalue(), getRadius());
	float xval = getXvalue();
}

#pragma endregion


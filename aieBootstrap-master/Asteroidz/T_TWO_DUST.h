#pragma once
#include "TIER_TWO.h"
class T_TWO_DUST : public TIER_TWO
{
public:
	T_TWO_DUST();
	T_TWO_DUST(float newX, float newY, float newRadius, int newHealth, int newColour,
		int newSouls, float newSpeed, int newAttackPower, int newAmountofMoves, int newMoveMultipler);
	~T_TWO_DUST();
};


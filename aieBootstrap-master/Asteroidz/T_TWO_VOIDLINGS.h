#pragma once
#include "TIER_TWO.h"
class T_TWO_VOIDLINGS:
	public TIER_TWO
{
public:
	T_TWO_VOIDLINGS();
	T_TWO_VOIDLINGS(float newX, float newY, float newRadius, int newHealth, int newColour,
		int newSouls, float newSpeed, int newAttackPowerint, int newAmountofMoves, int newMoveMultipler);
	~T_TWO_VOIDLINGS();
};


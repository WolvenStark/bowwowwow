#include "TIER_THREE.h"



TIER_THREE::TIER_THREE()
{
}

TIER_THREE::TIER_THREE(float newX, float newY, float newRadius, int newHealth, int newColour,
	int newSouls, float newSpeed, int newAttackPower, int newAmountofMoves, int newMultipler)
	: TIER_TWO (newX, newY, newRadius, newHealth, newColour, newSouls, newSpeed, newAttackPower, newAmountofMoves, newMultipler),
	MoveUp(false), MoveLeft(false), MoveDown(false), MoveRight(false) {
}


TIER_THREE::~TIER_THREE()
{
}

void TIER_THREE::setMovement(aie::Input *input, aie::EInputCodes upKey, aie::EInputCodes downKey, aie::EInputCodes leftKey, aie::EInputCodes rightKey) {
	if (input->isKeyDown(upKey))
		MoveUp = true;
	if (input->isKeyDown(leftKey))
		MoveLeft = true;
	if (input->isKeyDown(downKey))
		MoveDown = true;
	if (input->isKeyDown(rightKey))
		MoveRight = true;

	if (input->isKeyUp(upKey))
		MoveUp = false;
	if (input->isKeyUp(leftKey))
		MoveLeft = false;
	if (input->isKeyUp(downKey))
		MoveDown = false;
	if (input->isKeyUp(rightKey))
		MoveRight = false;
}

void TIER_THREE::updateMovement(float deltaTime) {
	// TODO: put screen bounds here
	
	if (getHealth() > 0) {
		if (MoveUp)
			setYvalue(getYvalue() + getSpeed()*deltaTime);// move the player up
		if (MoveDown)
			setYvalue(getYvalue() - getSpeed()*deltaTime);// move the player down
		if (MoveLeft)
			setXvalue(getXvalue() - getSpeed()*deltaTime);// move the player left
		if (MoveRight)
			setXvalue(getXvalue() + getSpeed()*deltaTime);// move the player right
	}
	else {
		setXvalue(-1); setYvalue(-1);
	}
}


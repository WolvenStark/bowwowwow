#pragma once
#include "TIER_TWO.h"

TIER_TWO::TIER_TWO() : TIER_ONE()
{
}

TIER_TWO::TIER_TWO(float newX, float newY, float newRadius, int newHealth, int newColour,
	int newSouls, float newSpeed, int newAttackPower, int newAmountofMoves, int newMultipler)
	: TIER_ONE(newX, newY, newRadius, newHealth, newColour),
	t_Souls(newSouls), t_Speed(newSpeed), t_Attackpower(newAttackPower), t_AmountofMoves(newAmountofMoves), t_MoveMultipler(newMultipler)//;
{}

TIER_TWO::~TIER_TWO()
{
}

int		TIER_TWO::getSouls()
{
	return t_Souls;
}

void	TIER_TWO::setSouls(int Fresh_Souls_Value)
{
	t_Souls = Fresh_Souls_Value;
}

float	TIER_TWO::getSpeed()
{
	return t_Speed;
}

void	TIER_TWO::setSpeed(float Fresh_Speed_Value)
{
	t_Speed = Fresh_Speed_Value;
}

int		TIER_TWO::getAttackPower()
{
	return t_Attackpower;
}

void	TIER_TWO::setAttackPower(int Fresh_AttackPower_Value)
{
	t_Attackpower = Fresh_AttackPower_Value;
}

int		TIER_TWO::getAmountofMoves()
{
	return t_AmountofMoves;
}

void	TIER_TWO::setAmountofMoves(int Fresh_AmountofMoves_Value)
{
	t_AmountofMoves = Fresh_AmountofMoves_Value;
}

int		TIER_TWO::getMoveMultipler()
{
	return t_MoveMultipler;
}

void	TIER_TWO::setMoveMultipler(int Fresh_Multipler_Value)
{
	t_MoveMultipler = Fresh_Multipler_Value;
}
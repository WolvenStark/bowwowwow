#pragma once
#include "TIER_ONE.h"
class TIER_TWO : public TIER_ONE
{
public:
	TIER_TWO();
	TIER_TWO(float newX, float newY, float newRadius, int newHealth, int newColour, 
		int newSouls, float newSpeed, int newAttackPower, int newAmountofMoves, int newMultipler );
	~TIER_TWO();
	int		getSouls();
	void	setSouls(int Fresh_Souls_Value);
	float	getSpeed();
	void	setSpeed(float Fresh_Speed_Value);
	int		getAttackPower();
	void	setAttackPower(int Fresh_AttackPower_Value);
	int		getAmountofMoves();
	void	setAmountofMoves(int Fresh_AmountofMoves_Value);
	int		getMoveMultipler();
	void	setMoveMultipler(int Fresh_Multipler_Value);

protected:
	int		t_Souls;
	float	t_Speed;
	int		t_Attackpower;
	int		t_AmountofMoves;
	int		t_MoveMultipler;



};
#include "T_TWO_DUST.h"



T_TWO_DUST::T_TWO_DUST()
{
}

T_TWO_DUST::T_TWO_DUST(float newX, float newY, float newRadius, int newHealth, int newColour,
	int newSouls, float newSpeed, int newAttackPower, int newAmountofMoves, int newMultipler)
	: TIER_TWO(newX, newY, newRadius, newHealth, newColour, newSouls, newSpeed, newAttackPower, newAmountofMoves, newMultipler)
{
}

T_TWO_DUST::~T_TWO_DUST()
{
}

#pragma once
#include "TIER_TWO.h"
#include "Input.h"

class TIER_THREE : public TIER_TWO
{
public:
	TIER_THREE();
	TIER_THREE(float newX, float newY, float newRadius, int newHealth, int newColour,
		int newSouls, float newSpeed, int newAttackPower, int newAmountofMoves, int newMultipler);
	~TIER_THREE();

	virtual void setMovement(aie::Input *input, aie::EInputCodes upKey, aie::EInputCodes downKey, aie::EInputCodes leftKey, aie::EInputCodes rightKey); 
	virtual void updateMovement(float deltaTime);
protected:

	bool MoveUp;
	bool MoveLeft;
	bool MoveDown;
	bool MoveRight;
};


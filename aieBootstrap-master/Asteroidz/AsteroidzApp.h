#pragma once

#include "Application.h"
#include "Renderer2D.h"
#include <vector>

#include "Input.h"
#include "T_TWO_DUST.h"
#include "T_TWO_VOIDLINGS.h"
#include "TIER_THREE.h"

using std::vector;

enum Colors
{
	CRIMSON			= 0xB0171FFF,		INDIAN_RED		= 0xB0171FFF,		GOLD			= 0xFFD700FF,	DK_GOLDEN_ROD = 0xB8860BFF,	
	SLATE_GREY3		= 0x9FB6CDFF,		SLATE_GREY		= 0x708090FF,		SPRING_GREEN	= 0x00FF7FFF,	ORANGE = 0xFFA500FF,
	SPRING_GREEN3	= 0x008B45FF,		CYAN2			= 0x00EEEEFF,		DK_CYAN			= 0x008B8BFF,	CUSTOM = 0x6b5c70FF,
	DODGER_BLUE		= 0x1E90FFFF,		DODGER_BLUE4	= 0x104E8BFF,		ORCHID			= 0x9400D3FF,
	SNOW			= 0xFFFAFAFF,		SNOW2			= 0xEEE9E9FF,		SNOW3			= 0xCDC9C9FF,	SNOW4 = 0x8B8989FF,
	WHITE_0			= 0xFFFFFFFF,		WHITE_2			= 0xF2F2F2FF,		WHITE_3			= 0xE6E6E6FF,	WHITE_4 =0xD9D9D9FF,
	WHITE_5			= 0xCCCCCCFF,		WHITE_6			= 0xBFBFBFFF,		WHITE_7			= 0xB3B3B3FF,	WHITE_8 = 0xA6A6A6FF,
	WHITE_9			= 0x999999FF,		WHITE_10		= 0x8C8C8CFF,
}; 

enum Vs { PvVS, PvAV, WSvVS, AvVS, AvAV };
//Vs whoisfighting = WSvVS;


class AsteroidzApp : public aie::Application {
public:

	AsteroidzApp();
	virtual ~AsteroidzApp() = default;  //In 2D space, used 3D vectors. In 3D space, use 4D vector.

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

	void testCreateAsteroid(float deltaTime);
	void testCreateVoidling(float deltaTime);
	float distance(float x1, float y1, float x2, float y2); //what is getYvalue() distance form objects
	
	void asteroidzUpdate(TIER_THREE*Sifbot,		TIER_THREE*Artoriasbot,		T_TWO_DUST *a_ast, float deltaTime);
	void voidlingsUpdate(TIER_THREE*Sifbot,		TIER_THREE*Artoriasbot,		T_TWO_VOIDLINGS*a_vdlgs, float deltaTime);
	void placepowerUpTicTacnPlay(); // If captured powerUpTicTac; move it, then reset it
	
	void sortAsteroidz();
	void sortVoidlings();

	void entity_Collision(TIER_THREE*player,	T_TWO_DUST*opponent,		float deltaTime); // Asteroid vs Player
	void entity_Collision(TIER_THREE*player,	T_TWO_VOIDLINGS*opponent,	float deltaTime); // Voidlings vs Player
	///void tic_Tac_Lottery();
	void createTempImage(float deltaTime);
	void displayingTempImage(aie::Renderer2D *a_r2d);
protected:

	bool	doOnce = true;
	bool	doTwice = true;
	bool	doThrice = true;
	bool	showImage1 = true;
	bool	showImage2 = false;
	bool	showImage3 = false;
	bool	showImage4 = false;
	bool	mGameIsOver;
	bool	mCombatIsOver;
	//bool	displaying_Intro_Screen_Movement;
	//bool	displaying_Intro_Screen_Enemy;
	//bool	displaying_Intro_Screen_Souls;
	bool	displaying_intro_graphics;
	float	mTimeIntervalT_TWO_DUST;
	float	mTimeIntervalVoidling;
	float	mTimer;

	//Screen Which_Screen = Screen_Asteroids;
	
	vector	<T_TWO_DUST>		dustparticle; //Must be able to sort many things. E.g based on health //Need 2 dynamic array (vectors)
	vector	<T_TWO_VOIDLINGS>	voidlinglist;
	TIER_ONE	tic_Tac;
	TIER_TWO	alpha_Voidling;
	TIER_THREE	player_Sif;
	TIER_THREE	player_Artorias;

	//aie::Texture*		m_texture;
	//aie::Texture*		m_SifTexture;
	aie::Texture*		m_Intro_Screen_Intro;
	aie::Texture*		m_Intro_Screen_Instructions;
	aie::Texture*		m_Intro_Screen_MoveKeys;
	aie::Texture*		m_Game_Screen_Background;
	aie::Renderer2D*	m_2dRenderer;
	aie::Font*			m_font;
};

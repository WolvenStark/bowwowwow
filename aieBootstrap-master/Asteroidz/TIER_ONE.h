#pragma once
#include "Renderer2D.h"

class TIER_ONE
{
public:
	TIER_ONE();
	TIER_ONE(float newX, float newY, float newRadius, int newHealth, int newColour);
	~TIER_ONE();
	float	getXvalue();
	void	setXvalue(		float Fresh_X_Value);
	float	getYvalue();
	void	setYvalue(		float Fresh_Y_Value);
	float	getRadius();
	void	setRadius(		float Fresh_Radius_Value);
	bool	isItAlive();
	void	setAliveStatus	(bool Fresh_IsItAlive_Value);
	int		getHealth()		const;
	void	healMe			(int amount_heal);
	void	damageMe		(int amount_damage);
	int		getColour();
	void	setColour(		int Fresh_Colour_Value);

	void render(aie::Renderer2D* a_r2d);

protected:
	float	t_X;
	float	t_Y;
	float	t_Radius;
	bool	t_AmIAlive;
	int		t_Health;
	int		t_MaxHealth;
	int		t_Colour;



};



#include "AsteroidzApp.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>
#include "T_TWO_DUST.h"
#include "T_TWO_VOIDLINGS.h"
#include "Input.h"
#include <time.h>
#include <vector>
#include <conio.h>
#include <time.h>

using namespace aie;
using std::vector;
using std::cout;
using std::endl;
using namespace::std;

AsteroidzApp::AsteroidzApp() { //SETUP
	dustparticle.clear();
	voidlinglist.clear();
	mGameIsOver = false;
	mCombatIsOver = true;
	mTimeIntervalT_TWO_DUST = 3.0f; // How often do the T_TWO_DUSTs spawn. Slow at the start to match Sif speed being slow. Oncoming Storm
	mTimeIntervalVoidling = 3.0f;

}

//AsteroidzApp::~AsteroidzApp() {
//
//}

#pragma region Core Functions
bool AsteroidzApp::startup() {

	m_2dRenderer = new aie::Renderer2D();
	m_font = new aie::Font("./font/consolas.ttf", 24);

	//Sprites will ALL be loaded here.
	m_Intro_Screen_Intro		= new aie::Texture("./Textures/menu_graphics_story.png");
	m_Intro_Screen_Instructions = new aie::Texture("./Textures/menu_graphics_instructions.png");
	m_Intro_Screen_MoveKeys		= new aie::Texture("./Textures/playermovement.jpg");
	m_Game_Screen_Background	= new aie::Texture("./Textures/menu_graphics_game_background.png");


	// Entities
	player_Sif = TIER_THREE((float)getWindowWidth() / 2, ((float)getWindowHeight() / 2) - 20, 25.f, 1000, CYAN2, 0, 120.f, 10, 5, 1);
	TIER_THREE *Sifbot;
	Sifbot = &player_Sif;

	player_Artorias = TIER_THREE((player_Sif.getXvalue()), (player_Sif.getYvalue() + (player_Sif.getRadius() * 2) + 20), 25.f, 500, CRIMSON, 0, 100.f, 15, 5, 1);
	TIER_THREE *Artoriasbot;
	Artoriasbot = &player_Artorias;

	tic_Tac = TIER_ONE(-1.f, -1.f, 10.f, 1, GOLD);
	alpha_Voidling = TIER_TWO(0.f, 0.f, 15.f, 1200, SPRING_GREEN, 0, 110.f, 12, 3, 1);

	//setup powerUpTicTac
	placepowerUpTicTacnPlay();
	mTimer = 0.f;
	displaying_intro_graphics = true;
	return true;
}

void AsteroidzApp::shutdown() {

	//delete m_texture;
	//delete m_SifTexture;
	delete m_font;
	delete m_2dRenderer;
	delete m_Intro_Screen_Intro;
	delete m_Intro_Screen_Instructions;
	delete m_Intro_Screen_MoveKeys;
	delete m_Game_Screen_Background;
}

void AsteroidzApp::update(float deltaTime) { //if game is over; do not update.  //called at approx. 60fps
	createTempImage(deltaTime);

	if (!displaying_intro_graphics) {
		aie::Input* input = aie::Input::getInstance();

		// input example
		if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))  ///ESC key // exit the application
			quit();

		if (mGameIsOver) return;
		mTimer += deltaTime;
		if (tic_Tac.getXvalue() == -1)
		{
			placepowerUpTicTacnPlay();
		}

		testCreateAsteroid(deltaTime); 	//do we create another T_TWO_DUST
		sortAsteroidz(); //Sorting, change to upon collison rather than upon every frame
		testCreateVoidling(deltaTime); //do we create another voidling. Maximum 20.
		sortVoidlings(); //Sorting,

		//___ UPDATE T_TWO_DUSTS __________
		for (unsigned int i = 0; i < dustparticle.size(); ++i) // for every asteroid based on size - change to health
		{
			asteroidzUpdate(&player_Sif, &player_Artorias, &dustparticle[i], deltaTime);
		}
		// now erase all T_TWO_DUSTs that are outside the screen
		for (unsigned int i = dustparticle.size(); i--;) {
			if (dustparticle[i].isItAlive() == true) continue;
			dustparticle.erase(dustparticle.begin() + i);
		}

		//___ UPDATE T_TWO_VOIDLINGS_______
		for (unsigned int j = 0; j < voidlinglist.size(); ++j) // for every Voidling based on Health
		{
			voidlingsUpdate(&player_Sif, &player_Artorias, &voidlinglist[j], deltaTime);
		}

		// now erase all T_TWO_VOIDLINGSthat are outside the screen
		for (unsigned int j = voidlinglist.size(); j--;) {
			if (voidlinglist[j].isItAlive() == true) continue;
			voidlinglist.erase(voidlinglist.begin() + j);
		}
		//____ SIF (Player ONE) Movement ____
		player_Sif.setMovement(input, aie::INPUT_KEY_W, aie::INPUT_KEY_S, aie::INPUT_KEY_A, aie::INPUT_KEY_D);
		player_Sif.updateMovement(deltaTime);
		//____ ARTORIAS (Player TWO) Movement ____
		player_Artorias.setMovement(input, aie::INPUT_KEY_UP, aie::INPUT_KEY_DOWN, aie::INPUT_KEY_LEFT, aie::INPUT_KEY_RIGHT);
		player_Artorias.updateMovement(deltaTime);
	}
} // If game is not paused loop 

void AsteroidzApp::draw() {
	//WOLFY

	clearScreen(); // wipe the screen to the background colour
	m_2dRenderer->begin();

	displayingTempImage(m_2dRenderer);
	if (!displaying_intro_graphics) {
#pragma region Drawing Sprites (Circles)
		if (!mGameIsOver) {
			// begin drawing sprites
			//-----------------------------------------------------------------
			// 1st = Asteroidz
			//-----------------------------------------------------------------

			for (unsigned int i = 0; i < dustparticle.size(); ++i)  //draw all asteroidz?
			{
				dustparticle[i].render(m_2dRenderer);
				int c = dustparticle[i].getColour();
			}

			//-----------------------------------------------------------------
			// 2nd = Sif
			//-----------------------------------------------------------------

			float xval = player_Sif.getXvalue();
			player_Sif.render(m_2dRenderer);

			//-----------------------------------------------------------------
			// 3rd = powerUpTicTac
			//-----------------------------------------------------------------

			if (tic_Tac.getXvalue() != -1) { //off
				tic_Tac.render(m_2dRenderer);

			}

			//-----------------------------------------------------------------
			// 4th = Artorias
			//-----------------------------------------------------------------
			if (player_Artorias.isItAlive() == false)
			{
				player_Artorias.setXvalue(-2);
				player_Artorias.setYvalue(-2);
				return;
			}
			else
			{
				player_Artorias.render(m_2dRenderer);
			}

			//-----------------------------------------------------------------
			// 6th = Voidlings
			//-----------------------------------------------------------------

			for (unsigned int j = 0; j < voidlinglist.size(); ++j)  //draw all voidlings
			{
				voidlinglist[j].render(m_2dRenderer);
			}
			//-----------------------------------------------------------------
#pragma endregion

#pragma region Draw Text
			if (mGameIsOver)
			{
				m_2dRenderer->setRenderColour(GOLD);				// another way
				std::ostringstream os;
				os << "Press ESC too quit";
				if (player_Sif.getSouls()) os << " [ " << player_Sif.getSouls() << " ] "; {
					m_2dRenderer->drawText(m_font, os.str().c_str(), 10, 10);     ///fix so that this code and section below are not repeats
				}
			}
			//else
			//{
			m_2dRenderer->setRenderColour(1.f, 1.f, 1.f, 0.4f); // transperent white 
			std::ostringstream os;
			os << "Sif's Souls:";
			os << " [ " << player_Sif.getSouls() << " ] "; 							//Display Sif's Score
			m_2dRenderer->drawText(m_font, os.str().c_str(), 10, 510);
			os.str("");
			os << "Artorias' Souls:";
			os << " [ " << player_Artorias.getSouls() << " ] "; {							//Display Artorias' Score
				m_2dRenderer->drawText(m_font, os.str().c_str(), 10, 480);
				os.str("");
				os << "Alpha Voidling's Souls:";
				os << " [ " << alpha_Voidling.getSouls() << " ] "; 					//Display Alpha Voidling's Score
				m_2dRenderer->drawText(m_font, os.str().c_str(), 10, 450);
				os.str("");
				os << "Sif's Health:";
				os << " [ " << player_Sif.getHealth() << " ] ";
				m_2dRenderer->drawText(m_font, os.str().c_str(), 10, 410);
				os.str("");
				os << "Artorias' Health";
				os << " [ " << player_Artorias.getHealth() << " ] ";
				m_2dRenderer->drawText(m_font, os.str().c_str(), 10, 380);
				os.str("");
				os << "Alpha Voidling's Health:";
				os << " [ " << alpha_Voidling.getHealth() << " ] ";
				m_2dRenderer->drawText(m_font, os.str().c_str(), 10, 350);
				//}
			}

		}// done drawing sprites
#pragma endregion
	}
	m_2dRenderer->end();

}
#pragma endregion

#pragma region Test Creation
void AsteroidzApp::testCreateAsteroid(float a_deltaTime) //do we create asteroidz (and do we hit the powerUpTicTac and claim a Souls)
{
	static float currentTime, stopCreateTime;
	currentTime += a_deltaTime;
	const float decreaseInterval = 2.0f;
	static float addTime;
	addTime += a_deltaTime;
	if (addTime > decreaseInterval) {
		addTime = 0;
		//lower the time of the next commet spwn, minimum cooldown 0.2f
		mTimeIntervalT_TWO_DUST = std::max(0.2f, mTimeIntervalT_TWO_DUST - 0.2f);
	}

	//have we hit the powerUpTicTac .... yeah
	if (distance(player_Sif.getXvalue(), player_Sif.getYvalue(), tic_Tac.getXvalue(), tic_Tac.getYvalue()) <= player_Sif.getRadius() + tic_Tac.getRadius()) // if the distance betweem two circles is less than both radius added together; they are touching
	{

		player_Sif.setRadius(std::max(tic_Tac.getRadius(), player_Sif.getRadius() - 1.f)); //radius decrease
		player_Sif.setSpeed(player_Sif.getSpeed() + 12.f); //speeds up... NEEeee oooOOHHM

		tic_Tac.setXvalue(-1.f);
		tic_Tac.setYvalue(-1.f);  // powerUpTicTac is out of play

		player_Sif.setSouls(player_Sif.getSouls() + 1);  //add different powerups and points  "Souls" and tic tacs

		stopCreateTime = 1.f; // stop asteroidz spawn
	}

	stopCreateTime = std::max(0.f, stopCreateTime - a_deltaTime);
	if (stopCreateTime) return;

	// think about creation. DO we want a T_TWO_DUST?
	if (currentTime < mTimeIntervalT_TWO_DUST) return;
	currentTime = 0;
	T_TWO_DUST particle;
	particle = T_TWO_DUST((float)(rand() % (getWindowWidth() - 50) + 25), (float)getWindowHeight() + 5,
		((450.0f / particle.getSpeed()) + 5.0f), int(particle.getRadius() * 2.0f),
		CRIMSON, 0, (float)(rand() % 600 + 50), 5, 5, 1);//int(particle.getRadius() / 5.0f));

	int color(int(particle.getSpeed() / 100)); // colour is based on intensity (speed)
	switch (color)
	{
	case 1: particle.setColour(WHITE_9); break;
	case 2: particle.setColour(WHITE_8); break;
	case 3: particle.setColour(WHITE_7); break;
	case 4: particle.setColour(WHITE_6); break;
	case 5: particle.setColour(WHITE_5); break;
	case 6: particle.setColour(WHITE_4); break;
	case 7: particle.setColour(WHITE_3); break;
	case 8: particle.setColour(WHITE_2); break;
	case 9: particle.setColour(WHITE_10); break;
	default: particle.setColour(WHITE_0); break;
	}
	dustparticle.push_back(particle);

}

void AsteroidzApp::testCreateVoidling(float a_deltaTime) //do we create asteroidz (and do we hit the powerUpTicTac and claim a Souls)
{
	static float currentTime, stopCreateTime;
	currentTime += a_deltaTime;
	const float decreaseInterval = 2.0f;
	static float addTime;
	addTime += a_deltaTime;
	if (addTime > decreaseInterval) {
		addTime = 0;
		//lower the time of the next voidling spwn, minimum cooldown 0.2f
		mTimeIntervalVoidling = std::max(0.2f, mTimeIntervalVoidling - 0.2f);
	}

	//has Artorias hit the powerUpTicTac .... yeah
	if (distance(player_Artorias.getXvalue(), player_Artorias.getYvalue(), tic_Tac.getXvalue(), tic_Tac.getYvalue()) <= player_Artorias.getRadius() + tic_Tac.getRadius()) // if the distance betweem two circles is less than both radius added together; they are touching
	{

		player_Artorias.setRadius(std::max(tic_Tac.getRadius(), player_Artorias.getRadius() - 1.f));
		player_Artorias.setSpeed(player_Artorias.getSpeed() + 12.f);
		tic_Tac.setXvalue(-1.f);
		tic_Tac.setYvalue(-1.f);  // powerUpTicTac is out of play
		player_Artorias.setSouls(player_Artorias.getSouls() + 1);  //add different powerups and points  "Souls" and tic tacs
		stopCreateTime = 1.f; // stop asteroidz spawn
	}

	stopCreateTime = std::max(0.f, stopCreateTime - a_deltaTime);
	if (stopCreateTime) return;

	// think about creation. DO we want a voidling?
	if (currentTime < mTimeIntervalVoidling) return;
	currentTime = 0;

	T_TWO_VOIDLINGS vd;
	vd = T_TWO_VOIDLINGS((float)(rand() % (getWindowWidth() - 50) + 20), ((float)getWindowHeight() + 5),
		5.0f, 18,
		ORCHID, 0, ((float)(rand() % 600 + 50)), 5, 5, 1);
	voidlinglist.push_back(vd);

}

float AsteroidzApp::distance(float x1, float y1, float x2, float y2)
{
	return sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2));  // x^2 + y^2
}

#pragma endregion

#pragma region Updates
void AsteroidzApp::asteroidzUpdate(TIER_THREE*Sifbot, TIER_THREE*Artoriasbot, T_TWO_DUST *a_ast, float a_deltaTime) // update one asteroid and check if we hit the Sif
{
	a_ast->setYvalue(a_ast->getYvalue() - a_ast->getSpeed()*a_deltaTime); // coming Down - Decreasing
	if (distance(player_Sif.getXvalue(), player_Sif.getYvalue(), a_ast->getXvalue(),
		a_ast->getYvalue()) < player_Sif.getRadius() + a_ast->getRadius())  //have we hit the Sif
	{
		entity_Collision(Sifbot, a_ast, a_deltaTime);
		//if (player_Sif.getHealth() <= 0)
		//{
		//	mGameIsOver = true;
		//}
	}

	if (distance(player_Artorias.getXvalue(), player_Artorias.getYvalue(), a_ast->getXvalue(),
		a_ast->getYvalue()) < player_Artorias.getRadius() + a_ast->getRadius()) //have we hit the Artorias
	{
		entity_Collision(Artoriasbot, a_ast, a_deltaTime);

		if (player_Artorias.getHealth() <= 0)
		{
			player_Artorias.setXvalue(-2);
			player_Artorias.setYvalue(-2);
			// ARTORIAS has died.
		}
		if (a_ast->getHealth() <= 0 || a_ast->getYvalue() < 0)
		{
			a_ast->setAliveStatus(false);
		}
	}

	if (a_ast->getYvalue() < 0) {		//are we below the screen
		a_ast->setAliveStatus(false);
	}
}

void AsteroidzApp::voidlingsUpdate(TIER_THREE*Sifbot, TIER_THREE*Artoriasbot, T_TWO_VOIDLINGS*a_vdlgs, float a_deltaTime)
{
	a_vdlgs->setYvalue(a_vdlgs->getYvalue() - a_vdlgs->getSpeed()*a_deltaTime); // coming Down - Decreasing
	if (distance(player_Sif.getXvalue(), player_Sif.getYvalue(), a_vdlgs->getXvalue(), a_vdlgs->getYvalue()) < player_Sif.getRadius() + a_vdlgs->getRadius())  //have we hit the Sif
	{
		//		int Testrandom = rand() % 20 + 1;
		//std::cout << "Random Number is " << Testrandom << endl;
		entity_Collision(Sifbot, a_vdlgs, a_deltaTime); //Sif vs the Voidling
	}

	if (distance(player_Artorias.getXvalue(), player_Artorias.getYvalue(), a_vdlgs->getXvalue(), a_vdlgs->getYvalue()) < player_Artorias.getRadius() + a_vdlgs->getRadius()) //have we hit the Artorias
	{

		entity_Collision(Artoriasbot, a_vdlgs, a_deltaTime);//Artorias vs the Voidling
		//player_Artorias.damageMe(a_vdlgs->getAttackPower());
		//a_vdlgs->damageMe(player_Artorias.getAttackPower());
		//if (player_Artorias.getHealth() <= 0)
		//{
		//	//player_Artorias.setHealth(0);
		//	player_Artorias.setXvalue(-2);
		//	player_Artorias.setYvalue(-2);
		//	//player_Artorias.setAliveStatus(false);
		//}
		//if (a_vdlgs->getHealth() <= 0)
		//{
		//	//a_vdlgs->setHealth(0);
		//	a_vdlgs->setAliveStatus(false);
		//}
	}

	if (a_vdlgs->getYvalue() < 0) {		//are we below the screen
		a_vdlgs->setAliveStatus(false);
	}
}

void AsteroidzApp::placepowerUpTicTacnPlay()
{
	if (tic_Tac.getXvalue() != -1.f) return; // Is the powerUpTicTac offscren.
	int countTries = 0;

	while (true)
	{
		if (countTries++ > 10000)
		{
			break;
		}
		float x = (float)(rand() % (getWindowWidth() - 40) + 20);
		float y = (float)(rand() % (getWindowHeight() - 40) + 20);

		// if the Sif is within the powerUpTicTac's radius placement zone
		if (distance(player_Sif.getXvalue(), player_Sif.getYvalue(), x, y) < player_Sif.getRadius() + 20)
		{
			continue;
		}

		// if the powerUpTicTac is within the powerUpTicTac's radius placement zone
		bool areaOk = true;
		for (unsigned int i = 0; i < dustparticle.size(); ++i)
		{
			if (distance(dustparticle[i].getXvalue(), dustparticle[i].getYvalue(), x, y) < dustparticle[i].getRadius() + 20)
			{
				areaOk = false;
			}
		}
		if (!areaOk)
		{
			continue;
		}
		//success
		tic_Tac.setXvalue(x);
		tic_Tac.setYvalue(y);
	}

}
#pragma endregion

#pragma region Sorting
void AsteroidzApp::sortAsteroidz() // sort --- because a sort routine is required. Vector Sort on !!!	health	!!!
{
	std::sort(dustparticle.begin(), dustparticle.end(), [](const T_TWO_DUST& a, const T_TWO_DUST& b)
	{
		return a.getHealth() < b.getHealth();
	});
}

void AsteroidzApp::sortVoidlings() // sort --- because a sort routine is required. Vector Sort on !!!	health	!!!
{
	std::sort(voidlinglist.begin(), voidlinglist.end(), [](const T_TWO_VOIDLINGS& a, const T_TWO_VOIDLINGS& b)
	{
		return a.getHealth() < b.getHealth();
	});
}
#pragma endregion

#pragma region Collisions [Debugging Exercise]
void AsteroidzApp::entity_Collision(TIER_THREE*player, T_TWO_VOIDLINGS*opponent, float deltaTime) //insert debugging code here.
{
	//-------------------------- Added code \/\ \/ \/ Debugging -------

	bool whose_turn = true;		 //< true = walker, false = Void
	cout << " " << flush;
	system("CLS");
	cout << "The two opponents circle one another; preparing to fight" << endl;
	int TurnsLeft_Combatant1 = (player->getAmountofMoves());
	int TurnsLeft_Combatant2 = (opponent->getAmountofMoves());

#pragma region Combatant One Turn

	while (TurnsLeft_Combatant1 > 0 && TurnsLeft_Combatant2 > 0) // If anyone has any moves left
	{
		if ((TurnsLeft_Combatant1 > 0) && whose_turn) // if there's at least one move left, and it is combatant one's turn to fight.
		{
			if (player->getHealth() > 0) {
				cout << "Combatant One attacks for " << player->getAttackPower() << " damage. " << endl;
				opponent->damageMe(player->getAttackPower());
				if (opponent->getHealth() <= 0)
				{
					cout << "Combatant One has " << TurnsLeft_Combatant1 << " moves left" << endl;
					cout << "The target dies" << endl;
					TurnsLeft_Combatant1--;
					cout << "					Currently Attacking : Combatant One " << endl;
					break;

				}
				else
				{
					cout << "Combatant One has " << TurnsLeft_Combatant1 << " moves left" << endl;
					cout << "The target did not die yet" << endl;
					TurnsLeft_Combatant1--;
					cout << "					Currently Attacking : Comabatant One" << endl;
				}
				whose_turn = false;
				continue;
			}
			else {
				break;
			}
		}
#pragma endregion

#pragma region Combatant Two Turn

		if ((TurnsLeft_Combatant2 > 0) && !whose_turn) {
			{
				if (opponent->getHealth() > 0) {

					cout << "Combatant Two attacks for " << opponent->getAttackPower() << " damage." << endl;
					player->damageMe(opponent->getAttackPower());
					if (player->getHealth() <= 0)
					{
						cout << "Combatant Two has " << TurnsLeft_Combatant2 << " moves left" << endl;
						cout << "The warriro succumbs to his wounds." << endl;
						TurnsLeft_Combatant2--;
						cout << "					Currently Attacking : Comabatant Two" << endl;
						break;

					}
					else
					{
						cout << "Combatant Two has " << TurnsLeft_Combatant2 << " moves left" << endl;
						cout << " Sif limps from her injuries" << endl;
						TurnsLeft_Combatant2--;
						cout << "					Currently Attacking : Comabatant Two" << endl;
					}
					whose_turn = true;
					continue;
				}
				else {
					continue;
				}
			}
			break;
			//-------------------------------------------------------------------------------------------------------
			// Once one team is completely eliminated, the fight ends and one team wins
			//-------------------------------------------------------------------------------------------------------
		}
		opponent->setAliveStatus(false);
	}
}

void AsteroidzApp::entity_Collision(TIER_THREE*player, T_TWO_DUST*opponent, float deltaTime) //insert debugging code here.
{
	//-------------------------- Added code \/\ \/ \/ Debugging -------

	bool whose_turn = true;		 //< true = walker, false = Void
	cout << " " << flush;
	system("CLS");
	cout << "The two opponents circle one another; preparing to fight" << endl;
	int TurnsLeft_Combatant1 = (player->getAmountofMoves());
	int TurnsLeft_Combatant2 = (opponent->getAmountofMoves());

#pragma region Combatant One Turn

	while (TurnsLeft_Combatant1 > 0 && TurnsLeft_Combatant2 > 0) // If anyone has any moves left
	{
		if ((TurnsLeft_Combatant1 > 0) && whose_turn) // if there's at least one move left, and it is combatant one's turn to fight.
		{
			if (player->getHealth() > 0) {
				cout << "Sif Attacks for " << player->getAttackPower() << " damage. " << endl;
				opponent->damageMe(player->getAttackPower());
				if (opponent->getHealth() <= 0)
				{
					cout << "Combatant One has " << TurnsLeft_Combatant1 << " moves left" << endl;
					cout << "The target dies" << endl;
					TurnsLeft_Combatant1--;
					cout << "					Currently Attacking : Combatant One " << endl;
					break;

				}
				else
				{
					cout << "Combatant One has " << TurnsLeft_Combatant1 << " moves left" << endl;
					cout << "The target did not die yet" << endl;
					TurnsLeft_Combatant1--;
					cout << "					Currently Attacking : Comabatant One" << endl;
				}
				whose_turn = false;
				continue;
			}
			else {
				break;
			}
		}
#pragma endregion

#pragma region Combatant Two Turn

		if ((TurnsLeft_Combatant2 > 0) && !whose_turn) {
			{
				if (opponent->getHealth() > 0) {

					cout << "A Voidling attacks for " << opponent->getAttackPower() << " damage." << endl;
					player->damageMe(opponent->getAttackPower());
					if (player->getHealth() <= 0)
					{
						cout << "Combatant Two has " << TurnsLeft_Combatant2 << " moves left" << endl;
						cout << " Sif succumbs to her wounds." << endl;
						TurnsLeft_Combatant2--;
						cout << "					Currently Attacking : Comabatant Two" << endl;
						break;

					}
					else
					{
						cout << "Combatant Two has " << TurnsLeft_Combatant2 << " moves left" << endl;
						cout << " Sif limps from her injuries" << endl;
						TurnsLeft_Combatant2--;
						cout << "					Currently Attacking : Comabatant Two" << endl;
					}
					whose_turn = true;
					continue;
				}
				else {
					continue;
				}
			}

			break;
			//-------------------------------------------------------------------------------------------------------
			// Once one team is completely eliminated, the fight ends and one team wins
			//-------------------------------------------------------------------------------------------------------
		}
		opponent->setAliveStatus(false);
	}
}


///void AsteroidzApp::tic_Tac_Lottery() //insert TTT code here, For collecting The Golden Lord souls.
///{
///
///}

#pragma endregion

#pragma region Intro Scene Graphics
void AsteroidzApp::createTempImage(float deltaTime) {
	static float currentTime;
	currentTime += deltaTime;

	if (currentTime < 5.f) return;
	doOnce = false;

	if (!doOnce) {
		showImage1 = false;
		showImage2 = true;
	}

	if (currentTime < 10.f) return;
	doTwice = false;

	if (!doTwice) {
		showImage2 = false;
		showImage3 = true;
	}

	if (currentTime < 15.f) return;
	doThrice = false;

	if (!doThrice) {
		showImage3 = false;
		showImage4 = true;
	}
	displaying_intro_graphics = false;
}

void AsteroidzApp::displayingTempImage(aie::Renderer2D *a_r2d)
{
	if (showImage1)
		a_r2d->drawSprite(m_Intro_Screen_Intro,				0, 0, float(getWindowWidth()), float(getWindowHeight()), 0, 0, 0, 0);
	if (showImage2)
		a_r2d->drawSprite(m_Intro_Screen_Instructions,		0, 0, float(getWindowWidth()), float(getWindowHeight()), 0, 0, 0, 0);
	if (showImage3)
		a_r2d->drawSprite(m_Intro_Screen_MoveKeys,			0, 0, float(getWindowWidth()), float(getWindowHeight()), 0, 0, 0, 0);
	if (showImage4)
		a_r2d->drawSprite(m_Game_Screen_Background,			0, 0, float(getWindowWidth()), float(getWindowHeight()), 0, 0, 0, 0);
}
#pragma endregion
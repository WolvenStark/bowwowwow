#include "Tank.h"
#include <Texture.h>
#include "SpriteNode.h"
#include "Input.h"

Tank::Tank()
{
	m_turretTexture		= new aie::Texture("./sceneGraph/textures/barrelGreen.png");
	m_shieldsTexture	= new aie::Texture("./sceneGraph/textures/shields.png");
	m_tankBaseTexture	= new aie::Texture("./sceneGraph/textures/tankBlue.png");
	
	//Turret Vector
	m_turret = new SpriteNode(m_turretTexture);
	m_turret->Translate(Vector2(0, 0));
	m_turret->SetOrigin(Vector2(0.0f, 0.5f));

	//Shield Vector
	m_shield = new SpriteNode(m_shieldsTexture);
	m_shield->Translate(Vector2(0.f, 0.f));
	m_shield->SetOrigin(Vector2(0.5f, 0.5f));

	//(Ship) Base Vector
	m_base = new SpriteNode(m_tankBaseTexture);
	m_base->Rotate(3.14159f / 4.0f); // 90 degrees

	//Parent setup
	m_turret->SetParent(m_base);
	m_shield->SetParent(m_base);
	m_base->SetParent(this);
}

Tank::~Tank()
{
	delete m_turret;
	delete m_shield;
	delete m_base;

	delete m_turretTexture;
	delete m_shieldsTexture;
	delete m_tankBaseTexture;

	//for (size_t i = 0; i < m_bullets.size(); ++i)
	//	delete m_bullets[i];
	//m_bullets.clear();
}

// We can use this function to clean up our bullets when they hit
// something by deleting all the 'dead' bullets
//void cleanupBullets() {
//	forloop...
//		if (bullets[i]->!isAlive())
//			// clean up the memory
//			delete m_bullets[i]
//			// and remove that empty memory from the vector
//			m_bullets.erase(m_bullets.begin() + i);
//}

void Tank::Update(float deltaTime)
{
	// Get a local instance of the input handler so that we
	// can perform checks for keypress, mouse movement
	// etc from input devices
	aie::Input* input = aie::Input::getInstance();

	if (input->isKeyDown(aie::INPUT_MOUSE_BUTTON_RIGHT)) {
		//	updateShooting(deltaTime);
		//	shoot();
		//}

		// update the bullets movement
	}
}

//void Tank::updateShooting(float deltaTime) {
//	// have a look at the timer for TestCreateAsteroids()
//	// and reset canshoot to true when the timer expires
//}

//void Tank::shoot() {
//	canShoot = false;
//	
//	SpriteNode *bullet = new SpriteNode(new aie::Texture("./textures/bullet.png"));
//	m_bullets.push_back(bullet);
//}

void Tank::Render(aie::Renderer2D *renderer)
{
	m_shield->Render(renderer);
	m_base->Render(renderer);
	m_turret->Render(renderer);
}

void Tank::SetTurretRotate(float p_radians)
{
	float bRads = m_base->GetLocalRotation();
	m_turret->setLocalMatrix(p_radians-bRads);
}

void Tank::SetShieldRotate(float p_radians)
{
	float bRads = m_base->GetLocalRotation();
	m_shield->setLocalMatrix(p_radians - bRads);

}
void Tank::SetBaseRotate(float p_radians)
{
	m_base->setLocalMatrix(p_radians);
}

void Tank::SetBaseTranslate(Vector2 p_move)
{
	m_base->Translate(p_move);
}

float Tank::GetBaseRotate()
{
	return m_base->GetLocalRotation();
}

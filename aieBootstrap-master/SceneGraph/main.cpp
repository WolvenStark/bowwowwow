#include "SceneGraphApp.h"
#include <GLFW\glfw3.h>

int main() {
	// we have to initialise glfw to use gl functions
	// for the window
	glfwInit(); 
	// disable window resizing
	//glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	
	auto app = new SceneGraphApp();
	//app->run("AIE", 1024, 768, false);
	app->run("AIE", 1920, 1080, true); // set fullscreen to true
	delete app;

	return 0;
}
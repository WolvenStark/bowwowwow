#pragma once

#include "Node.h"
#include <vector>

class SpriteNode;
namespace aie
{
	class Texture;
}

class Tank : public Node
{
public:

	Tank();
	virtual ~Tank();

	virtual void Update(float deltaTime);
	virtual void Render(aie::Renderer2D *renderer);
	virtual void SetTurretRotate(float p_radians);
	virtual void SetShieldRotate(float p_radians);
	virtual void SetBaseRotate(float p_radians);
	virtual void SetBaseTranslate(Vector2 p_move);
	virtual float GetBaseRotate();

	// We use this so that we can use and display our
	// bullets in the main application
//	std::vector<SpriteNode*> GetBullets();
//	void updateShooting(float deltaTime);
//	void shoot();

	// for collider boxes, have a look at aie::2DRenderer's
	// drawSpriteTransformed3x3() function (line 342 onwards...)

protected:
	SpriteNode *m_base;
	SpriteNode *m_turret;
	SpriteNode *m_shield;

	aie::Texture *m_tankBaseTexture = nullptr;
	aie::Texture *m_turretTexture	= nullptr;
	aie::Texture *m_shieldsTexture  = nullptr;

private:
	// Store a list of all the bullets we shoot
//	std::vector<SpriteNode*> m_bullets;

	bool canShoot = true, isShooting = false;
	float shootTimer = 0;
};
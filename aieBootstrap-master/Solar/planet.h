#pragma once

#include "Application.h"
#include "Renderer2D.h"
#include <vector>
#include "Matrix3.h"
#include "Vector3.h"
#include <memory>

using std::vector;

const float distanceScale = 0.00000001f;
enum Colors {
	RED = 0xff0000ff, DK_RED = 0x800000ff, YELLOW = 0xffff00ff, ORANGE = 0xff8000ff, GREY = 0xC0C0C0ff, DK_GREY = 0x808080ff, GREEN = 0x00ff00ff,
	DK_GREEN = 0x008000ff, CYAN = 0x00ffffff, DK_CYAN = 0x008080ff, BLUE = 0x0000ffff, DK_BLUE = 0x000080ff, WHITE = 0xffffffff, PURPLE = 0x8034f2,
	TRANS_RED = 0xff000040
};

class Moon
{
public:
	// Distance is in units of km (kilometers) and time is in units of earth days
	Moon(float distanceFromPlanet, float orbitTime, float rotationTime, float radius, aie::Texture*  textureHandle);

	// Calculate its position in 3d space relative to the planet in the orbit using the given time value
	//SHOULD BE UPDATE
	void calculatePosition(float deltaTime);

	// Calculate its position in 3d spacein the orbit using the given time value
	void update(float delatTime);

	// Render it to the screen
	void render(aie::Renderer2D*, Vector3 pos, float scale);

private:
	float	mdistanceFromPlanet;					// distance from its planet
	float	morbitTime;								// time it takes to complete 1 orbit
	float	mrotationTime; 							// time it takes to spin 360 degrees
	float	mradius;								// radius of the moon itself
	aie::Texture* mtextureHandle;					// the texture used for rendering
	Vector3 mposition;								// its position in 3d space relative to the planet
	Vector3 mcenter;								// its position in 2d space to the axis
	float	mrotation;								// its rotation around its axis
	float	mangle;									// current angle around the planet
};

class Planet
{
public:
	// Distance is in units of km (kilometers) and time is in units of earth days (365.25 orbit time for earth)
	Planet(float distanceFromSun, float orbitTime, float rotationTime, float radius, aie::Texture * textureHandle, Vector3 centre, int ID_Number, int Ring_Number);

	// Calculate its position in 3d spacein the orbit using the given time value
	void update(float delatTime, float a_scale, float width, float height);

	// Render it to the screen
	void render(aie::Renderer2D*, float scale);

	// Get its position in 3d world space units (after scaling) and put it into the 3d vector
	void getPosition(Vector3* vec);


	// add a moon to this planet
	void addMoon(float distanceFromPlanet, float orbitTime, float rotationTime, float radius, aie::Texture* textureHandle);

	float	getDistanceFromSun();
	void	setDistanceFromSun		(float Fresh_DistanceFromSun);
	float	getOrbitTime();
	void	setOrbitTime			(float Fresh_OrbitTime);
	float	getRotationTime();
	void	setRotationTime			(float Fresh_RotationTime);
	float	getRadius();
	void	SetRadius				(float Fresh_Radius);
	float	getAngle();
	void	SetAngle				(float Fresh_Angle);
	float	getRotation();
	void	setRotation				(float Fresh_Rotation);
	int		getID();

private:
	float distanceFromSun;									// distance from the sun
	float orbitTime;										// time it takes to complete 1 orbit
	float rotationTime;										// time it takes to spin 360 degrees
	float radius;											// radius of the planet itself
	float angle;											// current angle around the sun
	float rotation;											// its rotation around its axis
	int ID;
	int Ring_No;
	aie::Texture * textureHandle;							// the texture used for rendering
	Vector3 position;										// its position in 2d space
	std::vector<Moon> moons;								// list of moons attached to this planet
	Vector3 centre;											// its position in 2d space to the axis
	Vector3 alduincentre;

	void renderCircle(float centerX, float centerY, float radius, aie::Renderer2D * a_2dRenderer, Colors color = TRANS_RED, unsigned int sides = 100); // draw the orbit
};

class Entity
{
public:
private:
};
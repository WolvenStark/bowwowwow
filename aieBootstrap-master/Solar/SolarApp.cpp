#include "SolarApp.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include "ErrorManager.h"
#include "ResourceManager.h"


SolarApp::SolarApp() {
}

SolarApp::~SolarApp() {

}

bool SolarApp::startup() {
	m_cameraX = m_cameraY = 0;
	m_scale = 1;

	m_2dRenderer = std::unique_ptr<aie::Renderer2D>(new aie::Renderer2D);

	// Create a font using the resource manager for safe loading
	m_sunTex		= ResourceManager::loadSharedResource<aie::Texture>("./solar/textures/sun.png");
	m_mercuryTex	= ResourceManager::loadSharedResource<aie::Texture>("./solar/textures/mercury.png");
	m_venusTex		= ResourceManager::loadSharedResource<aie::Texture>("./solar/textures/venera.png");
	m_earthTex		= ResourceManager::loadSharedResource<aie::Texture>("./solar/textures/earth.png");
	m_marsTex		= ResourceManager::loadSharedResource<aie::Texture>("./solar/textures/mars.png");
	m_jupiterTex	= ResourceManager::loadSharedResource<aie::Texture>("./solar/textures/jupiter.png");
	m_saturnTex		= ResourceManager::loadSharedResource<aie::Texture>("./solar/textures/saturn.png");
	m_uranusTex		= ResourceManager::loadSharedResource<aie::Texture>("./solar/textures/uran.png");
	m_neptuneTex	= ResourceManager::loadSharedResource<aie::Texture>("./solar/textures/neptun.png");
	m_starsTex		= ResourceManager::loadSharedResource<aie::Texture>("./solar/textures/starground.png");
	//m_moonTex		= ResourceManager::loadSharedResource<aie::Texture>("./solar/textures/moon.png");

	// MUST BE LAST .. If we do this we have to handle memory manually - raw pointer shenanigans
	m_font = ResourceManager::loadSharedResource<aie::Font>("./solar/font/consolas.ttf", 32);

	//				Planet Distance	Distance to		Scale distance	Actual			Orbit times
	//				from Sun (AU)	planet			from Sun		diameter
	//								(kilometers)	(centimeters)	(kilometers)
	//	Sun(a star)		0										1391980
	//	Mercury			0.39		 58000000		3.9			4880				88
	//	Venus			0.72		108000000		7.9			12100				224.7
	//	Earth			1.00		150000000		10			12800				365.2
	//	Mars			1.52		228000000		15.2		6800				687.0
	//	Jupiter			5.20		778000000		52.0		142000				4332
	//	Saturn			9.54 1,		430000000		95.4		120000				10760
	//	Uranus			19.2 2,		870000000		192			51800				30700
	//	Neptune			30.1 4,		500000000		301			49500				60200
	//	Pluto			39.4 5,		900000000		394			2300				90600

	// and build the planets
	float orbitSlowdown = 5;

	//-----------HERE-
	//Vector3 center = { (float)getWindowWidth() / 2, (float)getWindowHeight() / 2, 1 };
	Vector3 center = { 500, 300, 1 };
	Vector3 alduincentre = { ((float)getWindowWidth() / 2)-250, ((float)getWindowHeight() / 2) +250, 1 };


	//planets.push_back(Planet(100000000, 400, 1.f, 695500, m_saturnTex.get(), center, 1, 1)); // player
	planets1.push_back(Planet(0.f,					0.f,		500.f,		695500, m_sunTex.get(),		center,1,1)); // sun
	
	//for (int i = 1; /*i=(rand()%5)*/i < 2; /*i+= (rand()%2?1:-1)*/i++)
	//{
		//planets.push_back(Planet((float)58000000,		88.f,		1.6f,		2440,	m_mercuryTex.get(),	center,i,2)); // mercury
		//planets.push_back(Planet((float)88000000,		90600.f,	1.6f,		2440,	m_mercuryTex.get(), center,i,-3)); // mercury
		//planets.push_back(Planet((float)108000000,		224.7f,		1.f,		6052,	m_venusTex.get(),	center,i,3)); // venus
		planets1.push_back(Planet((float)150000000,		365.2f,		1.f	,		6371,	m_earthTex.get(),	center,1,4)); // real earth
		planets1.push_back(Planet((float)228000000,		687.0f,		1.03f,		3389,	m_marsTex.get(),	center,1,5)); // mars legion
		planets1.push_back(Planet((float)778000000,		4332.f,		1.4139f,	69911,	m_jupiterTex.get(), center,1,-5)); // jupiter legion
		planets1.push_back(Planet((float)430000000,		10760.f,	1.f,		1137,	m_saturnTex.get(),	center,1,6)); // saturn
		planets1.push_back(Planet((float)870000000,		60200.f,	1.2f,		1137,	m_uranusTex.get(),	center,1,-7)); // Uranus
		planets1.push_back(Planet((float)500000000,		90600.f,	1.1f,		1137,	m_neptuneTex.get(), center,1,8)); // neptune
	//}

	//-----------not centre
	planets2.push_back(Planet(0.f, 0.f, 500.f, 695500, m_sunTex.get(), alduincentre, 2, 1)); // alduin sun

	//for (int i = 1; /*i=(rand()%5)*/i < 2; /*i+= (rand()%2?1:-1)*/i++)
	//{
		//planets.push_back(Planet((float)58000000,		88.f,		1.6f,		2440,	m_mercuryTex.get(),	center,i,2)); // mercury
		//planets.push_back(Planet((float)88000000,		90600.f,	1.6f,		2440,	m_mercuryTex.get(), center,i,-3)); // mercury
		//planets.push_back(Planet((float)108000000,		224.7f,		1.f,		6052,	m_venusTex.get(),	center,i,3)); // venus
		planets2.push_back(Planet((float)150000000, 365.2f,	1.f,		6371,	m_earthTex.get(), alduincentre, 2, 4)); // real earth
		planets2.push_back(Planet((float)228000000, 687.0f,	1.03f,		3389,	m_marsTex.get(), alduincentre, 2, 5)); // mars legion
		planets2.push_back(Planet((float)778000000, 4332.f,	1.4139f,	69911,	m_jupiterTex.get(), alduincentre, 2, -5)); // jupiter legion
		planets2.push_back(Planet((float)430000000, 10760.f, 1.f,		1137,	m_saturnTex.get(), alduincentre, 2, 6)); // saturn
		planets2.push_back(Planet((float)870000000, 60200.f, 1.2f,		1137,	m_uranusTex.get(), alduincentre, 2, -7)); // Uranus
		planets2.push_back(Planet((float)500000000, 90600.f, 1.1f,		1137,	m_neptuneTex.get(), alduincentre, 2, 8)); // neptune
	//}
	//--------------------

	//planets.push_back(Planet((float)778000000,		4332.f,		0.4139f,	69911,	m_jupiterTex.get(),	center,1,6)); // jupiter

	// add any moons
	//planets[0].addMoon(7000000, 27.3, 27.3, 1738, m_moonTex.get());

	return true;
}

void SolarApp::shutdown() {
	// now only delete fonts and sounds as smart pointers are in effect
	//delete m_font;
}

void SolarApp::update(float deltaTime) {
	static double lastMouseMove = 0;
	// input example
	aie::Input* input = aie::Input::getInstance();

	// new planets displayed
	// mouse wheel
	double movement = input->getMouseScroll();
	double difference = movement;
	m_scale = (m_scale - (float)(lastMouseMove - movement) / 50.f); // this is the original
																	// this is where is was initially
	if (m_scale < .2f) m_scale = 0.2f;
	lastMouseMove = movement;


	//Vector3 vec;
	for (unsigned int i = 0; i < planets1.size(); i++) {
	//	
	//	// (planets[i].getID == 1)
	//	//
		planets1[i].update(deltaTime, m_scale, getWindowWidth() / 2.0f, getWindowHeight() / 2.0f);
	//	planets[i].getPosition(&vec);
	//	m_scale = vec * (m_scale - (float)(lastMouseMove - movement) / 50.f);// this is the one I tweaked
	//	//}
	//	//else if (planets[i].getID == 2)
	//	//{
	//	//	
	//	//}
	}
	for (unsigned int i = 0; i < planets2.size(); i++) {
		planets2[i].update(deltaTime, m_scale, getWindowWidth() / 2.0f, getWindowHeight() / 2.0f);
	}


	if (input->isKeyDown(aie::INPUT_KEY_UP))
		m_cameraY += 500.0f * deltaTime;
		

	if (input->isKeyDown(aie::INPUT_KEY_DOWN))
		m_cameraY -= 500.0f * deltaTime;

	if (input->isKeyDown(aie::INPUT_KEY_LEFT))
		m_cameraX -= 500.0f * deltaTime;

	if (input->isKeyDown(aie::INPUT_KEY_RIGHT))
		m_cameraX += 500.0f * deltaTime;

	if (input->isKeyDown(aie::INPUT_KEY_C)) {
		m_cameraX = m_cameraY = 0;
		m_scale = 1.0f;

		//if (input->isKeyDown(aie::INPUT_KEY_W)) {
		//	}
		//if (input->isKeyDown(aie::INPUT_KEY_A))
		//{
		//	planets[0].SetAngle(-20);
		//}
		//if (input->isKeyDown(aie::INPUT_KEY_S)) {
		//
		//}
		//if (input->isKeyDown(aie::INPUT_KEY_D))
		//{
		//	planets[0].SetAngle(20);
		//}

		// exit the application
		if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
			quit();
	}
}


void SolarApp::draw() {

	// wipe the screen to the background colour
	clearScreen();

	m_2dRenderer->setCameraPos(m_cameraX, m_cameraY);

	// begin drawing sprites
	m_2dRenderer->begin();

	// draw a cross hairs
	m_2dRenderer->setRenderColour(1.f, 1.f, 0.f, 0.2f);
	m_2dRenderer->drawLine(0, (float)getWindowHeight() / 2, (float)getWindowWidth(), (float)getWindowHeight() / 2, 2, 1);
	m_2dRenderer->drawLine((float)getWindowWidth() / 2, 0, (float)getWindowWidth() / 2, (float)getWindowHeight(), 2, 1);

	// new planets displayed
	for (unsigned int i = 0; i < planets1.size(); i++) {
		planets1[i].render(m_2dRenderer.get(), m_scale);
	}
	for (unsigned int i = 0; i < planets2.size(); i++) {
		planets2[i].render(m_2dRenderer.get(), m_scale);
	}

	// output some text, uses the last used colour
	m_2dRenderer->setRenderColour(1.f, 1.f, 1.f, 0.2f);
#if 0
	char tempStr[256];
	sprintf_s(tempStr, "Press ESC to quit  %f scale = %f", (float)lastMouseMovePrint, m_scale);
	m_2dRenderer->drawText(m_font, tempStr, 0, 0);
#else
	m_2dRenderer->drawText(m_font.get(), "Press ESC to quit", 0, 0);
#endif

	// done drawing sprites
	m_2dRenderer->end();
}

float SolarApp::get_camX() 
{
	return m_cameraX;
}
float SolarApp::get_camY()
{
	return m_cameraY;
}


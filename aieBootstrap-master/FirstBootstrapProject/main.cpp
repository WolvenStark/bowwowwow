#include "FirstBootstrapProjectApp.h"

int main() {
	
	auto app = new FirstBootstrapProjectApp();
	app->run("AIE", 1280, 720, false);
	delete app;

	return 0;
}
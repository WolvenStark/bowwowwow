#include "FirstBootstrapProjectApp.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"

FirstBootstrapProjectApp::FirstBootstrapProjectApp() {

}

FirstBootstrapProjectApp::~FirstBootstrapProjectApp() {

}

bool FirstBootstrapProjectApp::startup() {
	
	m_2dRenderer = new aie::Renderer2D();
	m_font = new aie::Font("./font/consolas.ttf", 32);

	return true;
}

void FirstBootstrapProjectApp::shutdown() {

	delete m_font;
	delete m_2dRenderer;
}

void FirstBootstrapProjectApp::update(float deltaTime) {

	// input example
	aie::Input* input = aie::Input::getInstance();

	// exit the application
	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();
}

void FirstBootstrapProjectApp::draw() {

	// wipe the screen to the background colour
	clearScreen();

	// begin drawing sprites
	m_2dRenderer->begin();

	// draw your stuff here!
	m_2dRenderer->drawCircle(500.0f, 500.0f, 3.0f);
	
	// output some text, uses the last used colour
	m_2dRenderer->drawText(m_font, "Hello World", 0, 0);

	// done drawing sprites
	m_2dRenderer->end();
}